package me.timofeev.skyengdictionary.word_meaning.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class WordMeaningScope