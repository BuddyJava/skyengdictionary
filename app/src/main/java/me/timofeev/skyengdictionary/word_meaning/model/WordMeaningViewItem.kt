package me.timofeev.skyengdictionary.word_meaning.model

data class WordMeaningViewItem(
    val text: String,
    val translation: String,
    val imageUrl: String?,
)