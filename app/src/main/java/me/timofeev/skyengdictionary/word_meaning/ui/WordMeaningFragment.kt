package me.timofeev.skyengdictionary.word_meaning.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import me.timofeev.skyengdictionary.R
import me.timofeev.skyengdictionary.common.BaseMvpFragment
import me.timofeev.skyengdictionary.databinding.FragmentWordDetailBinding
import me.timofeev.skyengdictionary.di.Injector
import me.timofeev.skyengdictionary.word_meaning.di.DaggerWordMeaningComponent
import me.timofeev.skyengdictionary.word_meaning.di.WordMeaningComponent
import me.timofeev.skyengdictionary.word_meaning.di.WordMeaningModule
import me.timofeev.skyengdictionary.word_meaning.model.WordMeaningViewItem
import moxy.ktx.moxyPresenter
import javax.inject.Inject
import javax.inject.Provider

class WordMeaningFragment :
    BaseMvpFragment<WordMeaningComponent>(R.layout.fragment_word_detail),
    WordMeaningMvpView {

    private lateinit var binding: FragmentWordDetailBinding

    @Inject
    lateinit var presenterProvider: Provider<WordMeaningPresenter>

    private val presenter: WordMeaningPresenter by moxyPresenter { presenterProvider.get() }

    private val args: WordMeaningFragmentArgs by navArgs()

    override fun createDaggerComponent(): WordMeaningComponent {
        return DaggerWordMeaningComponent.builder()
            .appComponent(Injector.appComponent)
            .wordMeaningModule(WordMeaningModule(args.wordMeaningId))
            .build()
    }

    override fun inject(component: WordMeaningComponent) {
        component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentWordDetailBinding.bind(view)
        binding.swipeRefresh.setOnRefreshListener { presenter.onSwipeRefreshed() }
        binding.detailToolbar.setNavigationOnClickListener { activity?.onBackPressed() }
    }

    override fun showMeaning(meaning: WordMeaningViewItem) {
        Glide.with(binding.root.context)
            .load(meaning.imageUrl)
            .into(binding.wordImage)

        binding.detailToolbar.title = meaning.text
        binding.toolbarLayout.title = meaning.text
        binding.itemDetail.text = meaning.translation
    }

    override fun showProgress(visible: Boolean) {
        if (visible.not()) {
            binding.swipeRefresh.isRefreshing = false
        }
        binding.initialProgress.isVisible = visible && binding.swipeRefresh.isRefreshing.not()
        binding.swipeRefresh.isEnabled = binding.initialProgress.isVisible.not()
    }

    override fun showError(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT)
            .show()
    }
}