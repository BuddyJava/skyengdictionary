package me.timofeev.skyengdictionary.word_meaning.di

import dagger.Module
import dagger.Provides

@Module
class WordMeaningModule(private val meaningId: Int) {
    @Provides
    @WordMeaningScope
    @MeaningId
    fun provideMeaningId(): Int = meaningId
}