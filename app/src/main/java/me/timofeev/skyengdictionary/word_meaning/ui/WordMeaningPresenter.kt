package me.timofeev.skyengdictionary.word_meaning.ui

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import me.timofeev.skyengdictionary.R
import me.timofeev.skyengdictionary.common.BaseRxPresenter
import me.timofeev.skyengdictionary.common.ResourceManager
import me.timofeev.skyengdictionary.dictionary_service.DictionaryApi
import me.timofeev.skyengdictionary.word_meaning.di.MeaningId
import me.timofeev.skyengdictionary.word_meaning.model.WordMeaningViewItem
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution
import javax.inject.Inject

@AddToEndSingle
interface WordMeaningMvpView : MvpView {
    fun showMeaning(meaning: WordMeaningViewItem)
    fun showProgress(visible: Boolean)

    @OneExecution
    fun showError(message: String)
}

class WordMeaningPresenter @Inject constructor(
    @MeaningId private val meaningId: Int,
    private val dictionaryApi: DictionaryApi,
    private val resourceManager: ResourceManager
) : BaseRxPresenter<WordMeaningMvpView>() {

    private var refreshDisposable: Disposable? = null
        set(value) {
            field?.dispose()
            field = value
        }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        onSwipeRefreshed()
    }

    fun onSwipeRefreshed() {
        loadWordMeaning()
    }

    private fun loadWordMeaning() {
        refreshDisposable = dictionaryApi.wordInfo(meaningId)
            .compose(MeaningTransformer())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterSuccess { viewState.showProgress(false) }
            .subscribeBy(
                onSuccess = { result ->
                    viewState.showMeaning(result)
                },
                onError = { throwable ->
                    viewState.showError(resourceManager.getString(R.string.search_server_error))
                    throwable.printStackTrace()
                }
            )
    }
}