package me.timofeev.skyengdictionary.word_meaning.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class MeaningId
