package me.timofeev.skyengdictionary.word_meaning.di

import dagger.Component
import me.timofeev.skyengdictionary.di.components.AppComponent
import me.timofeev.skyengdictionary.word_meaning.ui.WordMeaningFragment

@Component(
    dependencies = [AppComponent::class],
    modules = [WordMeaningModule::class]
)
@WordMeaningScope
interface WordMeaningComponent {
    fun inject(wordMeaningFragment: WordMeaningFragment)
}