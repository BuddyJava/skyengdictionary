package me.timofeev.skyengdictionary.word_meaning.ui

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.SingleTransformer
import me.timofeev.skyengdictionary.dictionary_service.model.MeaningDTO
import me.timofeev.skyengdictionary.word_meaning.model.WordMeaningViewItem

class MeaningTransformer : SingleTransformer<List<MeaningDTO>, WordMeaningViewItem> {
    override fun apply(upstream: Single<List<MeaningDTO>>): SingleSource<WordMeaningViewItem> {
        return upstream
            .flatMapObservable { Observable.fromIterable(it) }
            .map {
                WordMeaningViewItem(
                    text = it.text,
                    translation = it.translation.text,
                    imageUrl = it.images.firstOrNull()?.formattedUrl
                )
            }
            .firstOrError()
    }
}