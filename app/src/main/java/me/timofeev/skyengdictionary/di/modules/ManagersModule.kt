package me.timofeev.skyengdictionary.di.modules

import dagger.Binds
import dagger.Module
import me.timofeev.skyengdictionary.common.ResourceManager
import me.timofeev.skyengdictionary.common.ResourceManagerImpl
import javax.inject.Singleton

@Module
abstract class ManagersModule {

    @Binds
    @Singleton
    abstract fun bindResourceManager(resourceManager: ResourceManagerImpl): ResourceManager

}