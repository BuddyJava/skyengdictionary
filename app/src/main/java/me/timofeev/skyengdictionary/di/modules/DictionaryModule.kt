package me.timofeev.skyengdictionary.di.modules

import dagger.Module
import dagger.Provides
import me.timofeev.skyengdictionary.dictionary_service.DictionaryApi
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DictionaryModule {

    @Provides
    @Singleton
    fun createDictionaryModule(retrofit: Retrofit): DictionaryApi {
        return retrofit.create(DictionaryApi::class.java)
    }
}