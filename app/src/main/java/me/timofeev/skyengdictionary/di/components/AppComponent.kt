package me.timofeev.skyengdictionary.di.components

import dagger.Component
import me.timofeev.skyengdictionary.common.ResourceManager
import me.timofeev.skyengdictionary.common.SchedulerProvider
import me.timofeev.skyengdictionary.di.modules.AppModule
import me.timofeev.skyengdictionary.di.modules.DictionaryModule
import me.timofeev.skyengdictionary.di.modules.ManagersModule
import me.timofeev.skyengdictionary.di.modules.NetworkModule
import me.timofeev.skyengdictionary.dictionary_service.DictionaryApi
import javax.inject.Singleton

@Component(
    modules = [
        AppModule::class,
        ManagersModule::class,
        NetworkModule::class,
        DictionaryModule::class,
    ]
)
@Singleton
interface AppComponent {
    fun getResourceManager(): ResourceManager
    fun getDictionaryApi(): DictionaryApi
    fun getSchedulers(): SchedulerProvider
}