package me.timofeev.skyengdictionary.di.modules

import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import me.timofeev.skyengdictionary.common.AppSchedulers
import me.timofeev.skyengdictionary.common.ResourceManager
import me.timofeev.skyengdictionary.common.ResourceManagerImpl
import me.timofeev.skyengdictionary.common.SchedulerProvider
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {
    @Provides
    @Singleton
    fun provideContext(): Context = context

    @Provides
    @Singleton
    fun provideSchedulersProvider(): SchedulerProvider = AppSchedulers()
}