package me.timofeev.skyengdictionary.di

import me.timofeev.skyengdictionary.di.components.AppComponent

object Injector {
    private var _appComponent: AppComponent? = null
    val appComponent: AppComponent
        get() = _appComponent ?: throw IllegalAccessError("AppComponent has not been initialized")

    fun setAppComponent(appComponent: AppComponent) {
        this._appComponent = appComponent
    }

}
