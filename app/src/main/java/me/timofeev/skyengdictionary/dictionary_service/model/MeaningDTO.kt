package me.timofeev.skyengdictionary.dictionary_service.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MeaningDTO(
    @SerialName("id") val id: Int,
    @SerialName("text") val text: String,
    @SerialName("translation") val translation: TranslationDTO,
    @SerialName("images") val images: List<ImageDTO>
)