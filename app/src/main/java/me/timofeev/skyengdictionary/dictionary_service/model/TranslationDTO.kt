package me.timofeev.skyengdictionary.dictionary_service.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TranslationDTO(
    @SerialName("text") val text: String
)