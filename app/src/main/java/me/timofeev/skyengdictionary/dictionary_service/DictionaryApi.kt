package me.timofeev.skyengdictionary.dictionary_service

import io.reactivex.Single
import me.timofeev.skyengdictionary.dictionary_service.model.MeaningDTO
import me.timofeev.skyengdictionary.dictionary_service.model.WordDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface DictionaryApi {

    @GET("/api/public/v1/words/search")
    fun searchWord(
        @Query("search") query: String,
        @Query("page") page: Int? = null,
        @Query("pageSize") size: Int? = null
    ): Single<List<WordDTO>>

    @GET("/api/public/v1/meanings")
    fun wordInfo(@Query("ids") wordMeaningId: Int): Single<List<MeaningDTO>>
}