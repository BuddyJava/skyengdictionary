package me.timofeev.skyengdictionary.dictionary_service.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WordDTO(
    @SerialName("id") val id: Int,
    @SerialName("text") val text: String,
    @SerialName("meanings") val meanings: List<MeaningLightDTO>
) {
    @Serializable
    data class MeaningLightDTO(
        @SerialName("id") val id: Int,
        @SerialName("translation") val translation: TranslationDTO
    )
}
