package me.timofeev.skyengdictionary.dictionary_service.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ImageDTO(
    @SerialName("url") private val _url: String
) {
    val formattedUrl: String = "https://$_url"
}