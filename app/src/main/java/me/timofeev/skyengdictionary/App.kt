package me.timofeev.skyengdictionary

import android.app.Application
import me.timofeev.skyengdictionary.di.Injector
import me.timofeev.skyengdictionary.di.components.DaggerAppComponent
import me.timofeev.skyengdictionary.di.modules.AppModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.setAppComponent(
            DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        )
    }
}