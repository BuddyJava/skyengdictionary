package me.timofeev.skyengdictionary.search_word.ui

import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.SingleTransformer
import me.timofeev.skyengdictionary.dictionary_service.model.WordDTO
import me.timofeev.skyengdictionary.search_word.model.SearchViewItem

class SearchViewItemTransformer : SingleTransformer<List<WordDTO>, List<SearchViewItem>> {
    override fun apply(upstream: Single<List<WordDTO>>): SingleSource<List<SearchViewItem>> {
        return upstream
            .map { words ->
                words.fold(mutableListOf(), { items, wordDTO ->
                    items.add(SearchViewItem.WordViewItem(wordDTO.id, wordDTO.text))
                    wordDTO.meanings.forEach { meaning ->
                        items.add(
                            SearchViewItem.MeaningViewItem(
                                meaning.id,
                                meaning.translation.text
                            )
                        )
                    }

                    items
                })
            }
    }
}