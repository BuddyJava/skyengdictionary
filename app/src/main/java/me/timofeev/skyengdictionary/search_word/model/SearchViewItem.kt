package me.timofeev.skyengdictionary.search_word.model

import androidx.recyclerview.widget.DiffUtil

sealed class SearchViewItem {
    abstract val id: Int
    abstract val text: String

    data class WordViewItem(
        override val id: Int,
        override val text: String
    ) : SearchViewItem()

    data class MeaningViewItem(
        override val id: Int,
        override val text: String
    ) : SearchViewItem()

    data class Progress(
        override val id: Int = Int.MAX_VALUE,
        override val text: String = ""
    ) : SearchViewItem()

    class DiffCallback : DiffUtil.ItemCallback<SearchViewItem>() {
        override fun areItemsTheSame(
            oldItem: SearchViewItem,
            newItem: SearchViewItem
        ): Boolean {
            return oldItem::class == newItem::class && oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: SearchViewItem,
            newItem: SearchViewItem
        ): Boolean {
            return oldItem::class == newItem::class && oldItem.text == newItem.text
        }
    }
}