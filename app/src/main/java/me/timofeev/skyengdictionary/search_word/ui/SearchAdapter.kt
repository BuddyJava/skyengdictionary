package me.timofeev.skyengdictionary.search_word.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import me.timofeev.skyengdictionary.common.PaginationViewHolder
import me.timofeev.skyengdictionary.search_word.model.SearchViewItem

internal class SearchAdapter(
    private val listener: OnMeaningClickListener
) : ListAdapter<SearchViewItem, RecyclerView.ViewHolder>(SearchViewItem.DiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            WORD_VIEW_TYPE -> WordSearchViewHolder.create(parent)
            MEANING_VIEW_TYPE -> MeaningSearchViewHolder.create(parent) { meaningViewItem ->
                listener.onMeaningClick(meaningViewItem)
            }
            PROGRESS_VIEW_TYPE -> PaginationViewHolder.create(parent)
            else -> throw IllegalAccessError("Unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val model = getItem(position)) {
            is SearchViewItem.MeaningViewItem -> (holder as MeaningSearchViewHolder).bind(model)
            is SearchViewItem.WordViewItem -> (holder as WordSearchViewHolder).bind(model)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is SearchViewItem.WordViewItem -> WORD_VIEW_TYPE
            is SearchViewItem.MeaningViewItem -> MEANING_VIEW_TYPE
            is SearchViewItem.Progress -> PROGRESS_VIEW_TYPE
        }
    }

    companion object {
        private const val WORD_VIEW_TYPE = 0
        private const val MEANING_VIEW_TYPE = 1
        private const val PROGRESS_VIEW_TYPE = 2
    }

    interface OnMeaningClickListener {
        fun onMeaningClick(meaningItem: SearchViewItem.MeaningViewItem)
    }
}