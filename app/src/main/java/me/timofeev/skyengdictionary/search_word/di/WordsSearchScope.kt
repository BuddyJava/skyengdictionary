package me.timofeev.skyengdictionary.search_word.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class WordsSearchScope
