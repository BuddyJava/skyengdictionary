package me.timofeev.skyengdictionary.search_word.ui

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.material.snackbar.Snackbar
import me.timofeev.skyengdictionary.R
import me.timofeev.skyengdictionary.common.BaseMvpFragment
import me.timofeev.skyengdictionary.common.ExtraPaddingViewHolder
import me.timofeev.skyengdictionary.common.PaginationScrollListener
import me.timofeev.skyengdictionary.databinding.FragmentWordSearchBinding
import me.timofeev.skyengdictionary.di.Injector
import me.timofeev.skyengdictionary.search_word.di.DaggerWordSearchComponent
import me.timofeev.skyengdictionary.search_word.di.WordSearchComponent
import me.timofeev.skyengdictionary.search_word.model.SearchViewItem
import moxy.ktx.moxyPresenter
import javax.inject.Inject
import javax.inject.Provider

class WordSearchFragment :
    BaseMvpFragment<WordSearchComponent>(R.layout.fragment_word_search),
    WordSearchMvpView {

    private lateinit var binding: FragmentWordSearchBinding

    @Inject
    internal lateinit var presenterProvider: Provider<WordSearchPresenter>

    private val searchPresenter by moxyPresenter { presenterProvider.get() }

    private val searchAdapter: SearchAdapter by lazy {
        SearchAdapter(object : SearchAdapter.OnMeaningClickListener {
            override fun onMeaningClick(meaningItem: SearchViewItem.MeaningViewItem) {
                findNavController().navigate(
                    WordSearchFragmentDirections.openDetailScreen(meaningItem.id)
                )
            }
        })
    }

    override fun createDaggerComponent(): WordSearchComponent {
        return DaggerWordSearchComponent.builder()
            .appComponent(Injector.appComponent)
            .build()
    }

    override fun inject(component: WordSearchComponent) {
        component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentWordSearchBinding.bind(view)

        binding.searchInput.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchPresenter.onSearchQueryChanged(query ?: "")
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                searchPresenter.onSearchQueryChanged(newText ?: "")
                return true
            }
        })

        with(binding.searchList) {
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    super.getItemOffsets(outRect, view, parent, state)
                    val viewHolder = parent.getChildViewHolder(view)
                    val horizontalPadding = if (viewHolder is ExtraPaddingViewHolder) {
                        resources.getDimensionPixelOffset(R.dimen.container_horizontal_margin) * 2
                    } else {
                        resources.getDimensionPixelOffset(R.dimen.container_horizontal_margin)
                    }

                    view.updatePadding(left = horizontalPadding, right = horizontalPadding)
                }
            })
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            adapter = searchAdapter

            addOnScrollListener(PaginationScrollListener { searchPresenter.onLoadMore() })
        }
    }

    override fun showSearchResult(result: List<SearchViewItem>) {
        binding.searchIdleText.isVisible = result.isEmpty()
        searchAdapter.submitList(result)
    }

    override fun showProgress(visible: Boolean) {
        binding.progress.isVisible = visible
    }

    override fun showMessage(text: String) {
        Snackbar.make(binding.coordinator, text, Snackbar.LENGTH_SHORT)
            .show()
    }
}