package me.timofeev.skyengdictionary.search_word.di

import dagger.Component
import me.timofeev.skyengdictionary.di.components.AppComponent
import me.timofeev.skyengdictionary.search_word.ui.WordSearchFragment

@Component(
    dependencies = [AppComponent::class],
)
@WordsSearchScope
interface WordSearchComponent {
    fun inject(wordSearchFragment: WordSearchFragment)
}