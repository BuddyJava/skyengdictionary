package me.timofeev.skyengdictionary.search_word.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.timofeev.skyengdictionary.common.ExtraPaddingViewHolder
import me.timofeev.skyengdictionary.databinding.ItemSearchMeaningBinding
import me.timofeev.skyengdictionary.search_word.model.SearchViewItem

internal class MeaningSearchViewHolder private constructor(
    private val binding: ItemSearchMeaningBinding,
    private val onClickListener: (SearchViewItem.MeaningViewItem) -> Unit
) : RecyclerView.ViewHolder(binding.root), ExtraPaddingViewHolder {

    private var model: SearchViewItem.MeaningViewItem? = null

    init {
        binding.root.setOnClickListener { model?.let(onClickListener) }
    }

    fun bind(model: SearchViewItem.MeaningViewItem) {
        this.model = model
        binding.title.text = model.text
    }

    companion object {
        fun create(
            parent: ViewGroup,
            onClickListener: (SearchViewItem.MeaningViewItem) -> Unit
        ): MeaningSearchViewHolder {
            return MeaningSearchViewHolder(
                binding = ItemSearchMeaningBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                ),
                onClickListener = onClickListener,
            )
        }
    }
}