package me.timofeev.skyengdictionary.search_word.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.timofeev.skyengdictionary.databinding.ItemSearchWordBinding
import me.timofeev.skyengdictionary.search_word.model.SearchViewItem

class WordSearchViewHolder private constructor(
    private val binding: ItemSearchWordBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: SearchViewItem.WordViewItem) {
        binding.title.text = model.text
    }

    companion object {
        fun create(parent: ViewGroup): WordSearchViewHolder {
            return WordSearchViewHolder(
                binding = ItemSearchWordBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }
    }
}