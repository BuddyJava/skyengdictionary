package me.timofeev.skyengdictionary.search_word.ui

import androidx.annotation.VisibleForTesting
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import me.timofeev.skyengdictionary.R
import me.timofeev.skyengdictionary.common.BaseRxPresenter
import me.timofeev.skyengdictionary.common.ResourceManager
import me.timofeev.skyengdictionary.common.SchedulerProvider
import me.timofeev.skyengdictionary.dictionary_service.DictionaryApi
import me.timofeev.skyengdictionary.search_word.model.SearchViewItem
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AddToEndSingle
interface WordSearchMvpView : MvpView {
    fun showSearchResult(result: List<SearchViewItem>)
    fun showProgress(visible: Boolean)

    @OneExecution
    fun showMessage(text: String)
}

class WordSearchPresenter @Inject constructor(
    private val dictionaryApi: DictionaryApi,
    private val resourceManager: ResourceManager,
    private val schedulers: SchedulerProvider
) : BaseRxPresenter<WordSearchMvpView>() {

    private var searchResult: List<SearchViewItem> = emptyList()
    private var isAvailablePagination = true
    private var isLoadingPage = true
    private val querySubject = BehaviorSubject.create<SearchPage>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        querySubject
            .debounce(DEBOUNCE_TIME_MILLIS, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .switchMap(::performSearch)
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = { state: SearchState -> state.renderToViewState() },
                onError = { throwable -> throwable.printStackTrace() }
            )
            .untilDestroy()
    }

    fun onSearchQueryChanged(query: String) {
        querySubject.onNext(SearchPage(query, INITIAL_PAGE))
    }

    fun onLoadMore() {
        val previewPage = querySubject.value
        if (previewPage != null && isAvailablePagination && isLoadingPage.not()) {
            querySubject.onNext(previewPage.copy(page = previewPage.page + 1))
        }
    }

    private fun SearchState.renderToViewState() {
        when (this) {
            SearchState.NewQuery -> {
                isLoadingPage = true
                isAvailablePagination = true

                viewState.showProgress(true)
                searchResult = emptyList()
            }
            is SearchState.Complete -> {
                isLoadingPage = false

                searchResult = searchResult + this.list

                viewState.showSearchResult(searchResult)
                viewState.showProgress(false)
                if (this.list.isEmpty()) {
                    viewState.showMessage(resourceManager.getString(R.string.search_server_error))
                }
            }
            is SearchState.Error -> {
                isLoadingPage = false
                viewState.showProgress(false)
                viewState.showSearchResult(searchResult)
                viewState.showMessage(this.message)
            }
            SearchState.PaginationStart -> {
                isLoadingPage = true
                viewState.showSearchResult(searchResult + listOf(SearchViewItem.Progress()))
            }
        }
    }

    @VisibleForTesting
    fun performSearch(searchPage: SearchPage): Observable<SearchState> {
        return dictionaryApi.searchWord(
            query = searchPage.query,
            page = searchPage.page,
            size = PAGE_SIZE
        )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSuccess { isAvailablePagination = it.size == PAGE_SIZE }
            .observeOn(schedulers.computation())
            .compose(SearchViewItemTransformer())
            .observeOn(schedulers.ui())
            .map<SearchState> { SearchState.Complete(it) }
            .onErrorReturn {
                it.printStackTrace()
                SearchState.Error(resourceManager.getString(R.string.search_server_error))
            }
            .toObservable()
            .startWith(
                if (searchPage.page > INITIAL_PAGE) SearchState.PaginationStart
                else SearchState.NewQuery
            )
    }

    data class SearchPage(
        val query: String,
        val page: Int = INITIAL_PAGE
    )

    sealed class SearchState {
        object NewQuery : SearchState()
        object PaginationStart : SearchState()
        data class Complete(val list: List<SearchViewItem>) : SearchState()
        data class Error(val message: String) : SearchState()
    }

    companion object {
        @VisibleForTesting
        const val DEBOUNCE_TIME_MILLIS = 300L

        @VisibleForTesting
        const val PAGE_SIZE = 20

        @VisibleForTesting
        const val INITIAL_PAGE = 1
    }
}