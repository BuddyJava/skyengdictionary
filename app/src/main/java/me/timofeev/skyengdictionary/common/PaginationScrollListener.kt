package me.timofeev.skyengdictionary.common

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

class PaginationScrollListener(
    private val onLoadMoreListener: () -> Unit
) : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val visibleItemCount = recyclerView.childCount
        val totalItemCount = recyclerView.layoutManager?.itemCount ?: 0

        val firstVisibleItemPosition: Int = if (recyclerView.layoutManager is LinearLayoutManager) {
            (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        } else if (recyclerView.layoutManager is StaggeredGridLayoutManager) {
            // https://code.google.com/p/android/issues/detail?id=181461
            if (recyclerView.layoutManager?.childCount ?: 0 > 0) {
                (recyclerView.layoutManager as StaggeredGridLayoutManager).findFirstVisibleItemPositions(
                    null
                )[0]
            } else {
                0
            }
        } else {
            throw IllegalStateException("LayoutManager needs to subclass LinearLayoutManager or StaggeredGridLayoutManager")
        }
        if ((totalItemCount - visibleItemCount) <= (firstVisibleItemPosition + 5)
            || totalItemCount == 0
        ) {
            onLoadMoreListener.invoke()
        }
    }
}