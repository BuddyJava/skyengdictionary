package me.timofeev.skyengdictionary.common

import android.content.Context
import androidx.annotation.StringRes
import javax.inject.Inject

interface ResourceManager {
    fun getString(@StringRes id: Int): String
}

class ResourceManagerImpl @Inject constructor(
    private val context: Context
) : ResourceManager {
    override fun getString(@StringRes id: Int): String = context.resources.getString(id)
}