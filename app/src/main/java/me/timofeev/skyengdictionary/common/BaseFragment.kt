package me.timofeev.skyengdictionary.common

import android.os.Bundle
import androidx.annotation.LayoutRes
import moxy.MvpAppCompatFragment

abstract class BaseMvpFragment<Component : Any>(@LayoutRes layoutRes: Int = 0) :
    MvpAppCompatFragment(layoutRes) {

    private val componentHolder = ComponentHolder<Component>(objectScopeName())

    abstract fun createDaggerComponent(): Component
    abstract fun inject(component: Component)

    override fun onCreate(savedInstanceState: Bundle?) {
        componentHolder.onCreate(savedInstanceState) { createDaggerComponent() }
        inject(componentHolder.component)

        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        componentHolder.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        componentHolder.onSaveInstanceState(outState, this)
    }

    override fun onDestroy() {
        super.onDestroy()
        componentHolder.onDestroy(this)
    }

    private fun Any.objectScopeName(): String = "${javaClass.simpleName}_${hashCode()}"
}