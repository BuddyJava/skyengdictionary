package me.timofeev.skyengdictionary.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.timofeev.skyengdictionary.databinding.ItemProgressBinding

class PaginationViewHolder(binding: ItemProgressBinding) : RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup) = PaginationViewHolder(
            binding = ItemProgressBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
}