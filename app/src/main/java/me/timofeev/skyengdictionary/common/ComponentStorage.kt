package me.timofeev.skyengdictionary.common

object ComponentStorage {
    private val componentsMap = mutableMapOf<String, Any>()

    fun getComponent(key: String): Any? = componentsMap[key]
    fun putComponent(key: String, component: Any) {
        componentsMap[key] = component
    }

    fun removeComponent(key: String): Any? = componentsMap.remove(key)
}