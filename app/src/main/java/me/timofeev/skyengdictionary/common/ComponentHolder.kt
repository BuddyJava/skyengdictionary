package me.timofeev.skyengdictionary.common

import android.os.Bundle
import android.os.Process
import android.util.Log
import androidx.fragment.app.Fragment
import me.timofeev.skyengdictionary.di.Injector

class ComponentHolder<Component : Any>(private val scopeName: String) {

    private var instanceStateSaved: Boolean = false

    lateinit var component: Component
    lateinit var fragmentComponentKey: String

    private val appCode: Int
        get() = Process.myPid()

    fun onCreate(
        savedInstanceState: Bundle?,
        componentInstaller: () -> Component
    ) {
        fragmentComponentKey = savedInstanceState?.getString(STATE_SCOPE_NAME) ?: scopeName
        component = ComponentStorage.getComponent(fragmentComponentKey)
            ?.run {
                Log.d(TAG, "Get exist Dagger component: $fragmentComponentKey")
                this as Component
            } ?: componentInstaller.invoke()
            .apply {
                Log.d(TAG, "Init new Dagger component: $fragmentComponentKey")
                ComponentStorage.putComponent(fragmentComponentKey, this)
            }
    }

    fun onResume() {
        instanceStateSaved = false
    }

    fun onSaveInstanceState(outState: Bundle, fragment: Fragment) {
        instanceStateSaved = true
        outState.putString(STATE_SCOPE_NAME, fragmentComponentKey)
        outState.putInt(STATE_LAUNCH_FLAG, appCode)
        outState.putBoolean(
            STATE_SCOPE_WAS_CLOSED,
            fragment.needCloseScope
        ) //save it but will be used only if destroyed
    }

    fun onDestroy(fragment: Fragment) {
        if (fragment.needCloseScope) {
            //destroy this fragment with scope
            Log.d(TAG, "Destroy Dagger component: $fragmentComponentKey")
            ComponentStorage.removeComponent(fragmentComponentKey)
        }
    }

    //This is android, baby!
    private val Fragment.isRealRemoving: Boolean
        get() = (this.isRemoving && !instanceStateSaved) //because isRemoving == true for fragment in backstack on screen rotation
                || (this.parentFragment?.isRealRemoving ?: false)

    //It will be valid only for 'onDestroy()' method
    private val Fragment.needCloseScope: Boolean
        get() = when {
            this.activity?.isChangingConfigurations == true -> false
            this.activity?.isFinishing == true -> true
            else -> this.isRealRemoving
        }

    companion object {
        const val STATE_SCOPE_NAME = "state_scope_name"
        const val STATE_LAUNCH_FLAG = "state_launch_flag"
        const val STATE_SCOPE_WAS_CLOSED = "state_scope_was_closed"
        private const val TAG = "ComponentHolder"
    }
}