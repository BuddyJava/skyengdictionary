package me.timofeev.skyengdictionary.search_word.ui

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import me.timofeev.skyengdictionary.common.ResourceManager
import me.timofeev.skyengdictionary.common.SchedulerProvider
import me.timofeev.skyengdictionary.dictionary_service.DictionaryApi
import me.timofeev.skyengdictionary.dictionary_service.model.WordDTO
import me.timofeev.skyengdictionary.search_word.model.SearchViewItem
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class WordSearchPresenterTest {

    private val resourceManager: ResourceManager = mock()
    private val dictionaryApi: DictionaryApi = mock()
    private val viewState: `WordSearchMvpView$$State` = mock()

    private lateinit var presenter: WordSearchPresenter

    @Before
    fun setUp() {
        presenter = WordSearchPresenter(
            dictionaryApi = dictionaryApi,
            resourceManager = resourceManager,
            schedulers = object : SchedulerProvider {
                override fun ui(): Scheduler = Schedulers.trampoline()
                override fun computation(): Scheduler = Schedulers.trampoline()
                override fun trampoline(): Scheduler = Schedulers.trampoline()
                override fun newThread(): Scheduler = Schedulers.trampoline()
                override fun io(): Scheduler = Schedulers.trampoline()
            }
        )

        presenter.setViewState(viewState)
    }

    @After
    fun tearDown() {
        presenter.onDestroy()
    }

    private val mockMessageResponse = "Test message"
    private val mockSearchPage = WordSearchPresenter.SearchPage("test")

    @Test
    fun `new query failed`() {
        whenever(
            dictionaryApi.searchWord(
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt()
            )
        )
            .thenReturn(Single.error(UnknownError("Test error")))

        whenever(resourceManager.getString(Mockito.anyInt()))
            .thenReturn(mockMessageResponse)

        presenter.performSearch(mockSearchPage)
            .test()
            .assertNoErrors()
            .assertValues(
                WordSearchPresenter.SearchState.NewQuery,
                WordSearchPresenter.SearchState.Error(mockMessageResponse),
            )
    }

    @Test
    fun `new query complete`() {
        val list = listOf(WordDTO(1, "", emptyList()))
        whenever(
            dictionaryApi.searchWord(
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt()
            )
        )
            .thenReturn(Single.just(list))

        presenter.performSearch(mockSearchPage)
            .test()
            .assertNoErrors()
            .assertValues(
                WordSearchPresenter.SearchState.NewQuery,
                WordSearchPresenter.SearchState.Complete(
                    listOf(
                        SearchViewItem.WordViewItem(1, "")
                    )
                ),
            )
    }
}