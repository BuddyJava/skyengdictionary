# SkyengDictionary

Тестовое приложение поиска английских слов, загрузку данных производить при помощи публичного API https://dictionary.skyeng.ru/doc/api/external.

## Сборка

После выполнения команды `./gradlew assembleDebug` сборка находится в `app/build/outputs/apk`


| Зависимости | Назначение |
|  :--: | :--: |
| RxJava | Работа с потоками |
| Dagger | DI framework  |
| Moxy | Библиотека для реализации MVP |
| Retrofit | Http клиент |
| Kotlin serialization | Json конвертор, с поддержкой дефолтных значений |
| Glide | Framework для работы с картинками |
| JetpackNavigation | Визуализация графа переходов, контроль аргументов фрагментов |
| Mockito-Kotlin | Библиотека расшерения для тестирования Kotlin |

--------

| Список | Детальный экран |
|  :--: | :--: |
|  ![List](https://bitbucket.org/BuddyJava/skyengdictionary/downloads/photo_2021-08-17_15-37-45.jpg) | ![Detail](https://bitbucket.org/BuddyJava/skyengdictionary/downloads/photo_2021-08-17_15-37-48.jpg) |



